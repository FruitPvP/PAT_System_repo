{
    "name": "1K",
    "planets": [
        {
            "name": "1K",
            "mass": 5000,
            "position_x": 0,
            "position_y": 25000,
            "velocity_x": -141.42135620117188,
            "velocity_y": -0.000006181723165354924,
            "required_thrust_to_move": 1000,
            "starting_planet": true,
            "respawn": false,
            "start_destroyed": false,
            "min_spawn_delay": 0,
            "max_spawn_delay": 0,
            "planet": {
                "seed": 1651043072,
                "radius": 1000,
                "heightRange": 0,
                "waterHeight": 0,
                "waterDepth": 100,
                "temperature": 100,
                "metalDensity": 100,
                "metalClusters": 100,
                "metalSpotLimit": -1,
                "biomeScale": 50,
                "biome": "asteroid",
                "symmetryType": "none",
                "symmetricalMetal": false,
                "symmetricalStarts": false,
                "numArmies": 3,
                "landingZonesPerArmy": 0,
                "landingZoneSize": 0
            }
        }
    ]
}